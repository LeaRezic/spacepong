package utils;

import javax.naming.*;
import java.util.Hashtable;

public class JndiUtil {

    public String getFileFromFS(String directoryFromSrc, String fileName) {
        String path = getAbsolutePathToFile(directoryFromSrc);
        Hashtable<String, String> env = new Hashtable<>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.fscontext.RefFSContextFactory");
        env.put(Context.PROVIDER_URL, path);

        try {
            Context ctx = new InitialContext(env);
            NamingEnumeration files = ctx.listBindings("");
            Binding file;
            while (files.hasMore()) {
                file = (Binding) files.next();
                String name = file.getName().split("\\.")[0];
                if (name.equals(fileName)) {
                    return file.getObject().toString();
                }
            }
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getAbsolutePathToFile(String directoryFromSrc) {
        return getClass().getClassLoader().getResource(directoryFromSrc).toExternalForm();
    }
}
