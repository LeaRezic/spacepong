package utils;

import javafx.application.Platform;
import javafx.scene.control.Label;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class TimerSocketListener implements Runnable {

    private Label timerLabel;

    public TimerSocketListener(Label timerLabel) {
        this.timerLabel = timerLabel;
    }

    @Override
    public void run() {
        MulticastSocket socket = null;
        try {
            String address = ReadConfigsUtil.readStringFromTextFile("groupSocketAddress");
            int port = ReadConfigsUtil.readIntFromTextFile("groupSocketPort");
            socket = new MulticastSocket(port);
            InetAddress group = InetAddress.getByName(address);
            socket.joinGroup(group);
            while(true) {
                DatagramPacket packet = receive(socket);
                String time = new String(packet.getData(), 0, packet.getLength());
                Platform.runLater(() -> timerLabel.setText(time));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static DatagramPacket receive(DatagramSocket socket) throws IOException {
        byte[] buf = new byte[1024];
        DatagramPacket p = new DatagramPacket(buf, buf.length);
        socket.receive(p);
        return p;
    }

}
