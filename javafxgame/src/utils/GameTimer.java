package utils;

import server.GameServer;

import java.rmi.RemoteException;

public class GameTimer implements Runnable {
    private GameServer server;
    private boolean shouldRun;
    private volatile boolean isPaused;
    private final Object notifier;
    private long elapsedMillis;

    private String currentTime;

    public GameTimer(GameServer server) {
        this.server = server;
        shouldRun = true;
        isPaused = true;
        elapsedMillis = 0;
        notifier = new Object();
        currentTime = "";
    }

    public void stop() {
        isPaused = true;
    }

    public void start() {
        synchronized (notifier) {
            isPaused = false;
            notifier.notify();
        }
    }

    public void shutDown() {
        shouldRun = false;
    }

    @Override
    public void run() {
        while (shouldRun) {
            synchronized (notifier) {
                while (isPaused) {
                    try {
                        notifier.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                elapsedMillis += 100;
                int min = (int)(elapsedMillis / 60000) % 60;
                int sec = (int)(elapsedMillis / 1000) % 60;
                int dec = (int)(elapsedMillis / 100) % 60;
                String time = getDisplayTime(min) + ":" + getDisplayTime(sec) + ":" + getDisplayTime(dec);

                try {
                    this.currentTime = time;
                    server.dispatchMatchTime(time);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String getDisplayTime(int time) {
        if (time < 10) {
            return "0" + time;
        }
        return String.valueOf(time);
    }

    public String getGameTime() {
        return currentTime;
    }
}
