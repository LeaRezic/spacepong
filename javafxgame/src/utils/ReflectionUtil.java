package utils;

import client.ScreenManager;
import game.GameState;
import game.gameObjects.*;
import client.GUI.GameScreen;

import java.io.*;
import java.lang.reflect.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ReflectionUtil {

    public ReflectionUtil() {}

    public void logAllObjectsDetails() {
        logClassDetails(new MovingObject());
        logClassDetails(new Paddle());
        logClassDetails(new Ball());
        logClassDetails(new Player());
        logClassDetails(new GameState());
        logClassDetails(new GameScreen());
    }

    public void generateHtmlDetails() {
        Object[] objects = {new MovingObject(), new Paddle(), new Ball(), new Player(), new ScreenManager(), new GameState(), new GameScreen(), new ArrayList<>(), new Object(), new StringBuilder()};
        String details = getHtmlObjectsReport(objects);
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("details.html"), StandardCharsets.UTF_8))) {
            writer.write(details);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void logClassDetails(Object object) {
        Class c = object.getClass();
        System.out.println("\n\n");
        System.out.println("CLASS: " + c.getName());
        System.out.println("-------------------------------------");
        System.out.println("Modifiers: ");
        getModifiersForClass(c).forEach(m -> System.out.println("*" + m));
        System.out.println("-------------------------------------");
        System.out.println("SuperClasses: ");
        getSuperClasses(object).forEach(s -> System.out.println("*" + s));
        System.out.println("-------------------------------------");
        System.out.println("Interfaces: ");
        getInterfaces(c).forEach(i -> System.out.println("*" + i));
        System.out.println("-------------------------------------");
        System.out.println("Declared Fields: ");
        getDeclaredFields(c).forEach(f -> System.out.println("*" + f));
        System.out.println("-------------------------------------");
        System.out.println("Constructors: ");
        getConstructors(c).forEach(cn -> System.out.println("*" + cn));
        System.out.println("-------------------------------------");
        System.out.println("Declared Methods: ");
        getDeclaredMethods(c).forEach(m -> System.out.println("*" + m));
    }

    private String getHtmlObjectsReport(Object[] objects) {
        StringBuilder sb = new StringBuilder();
        sb.append("<html><head><link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\"></head><body>");
        sb.append("<div class='p-5'>");
        sb.append("<h1 class='text-primary text-muted font-weight-bold'>REFLECTION</h1><br/><br/>");
        for (Object object : objects) {
            sb.append("<div>");
            sb.append(getHtmlObjectDetails(object));
            sb.append("</div>");
        }
        sb.append("</div></body></html>");
        return sb.toString();
    }

    private String getHtmlObjectDetails(Object object) {
        StringBuilder sb = new StringBuilder();
        Class c = object.getClass();
        sb.append("<div class='card m-3'>");
        sb.append("<div class='card-body'>");
        sb.append("<h2 class='card-title text-success'>").append(c.getSimpleName()).append("</h2><hr/>");
        sb.append("<div class='card-text'>");
        List<String> modifiers = getModifiersForClass(c);
        String segment = modifiers.isEmpty() ? "<h4>No modifiers</h4>" : getUl("Modifiers", modifiers);
        sb.append(segment);
        List<String> superClasses = getSuperClasses(object);
        segment = superClasses.isEmpty() ? "<h4>No SuperClasses</h4>" : getUl("SuperClasses", superClasses);
        sb.append(segment);
        List<String> interfaces = getInterfaces(c);
        segment = interfaces.isEmpty() ? "<h4>No interfaces</h4>" : getUl("Interfaces", interfaces);
        sb.append(segment);
        List<String> declaredFields = getDeclaredFields(c);
        segment = declaredFields.isEmpty() ? "<h4>No declared fields</h4>" : getUl("Declared Fields", declaredFields);
        sb.append(segment);
        List<String> constructors = getConstructors(c);
        segment = constructors.isEmpty() ? "<h4>No constructors</h4>" : getUl("Constructors", constructors);
        sb.append(segment);
        List<String> declaredMethods = getDeclaredMethods(c);
        segment = declaredMethods.isEmpty() ? "<h4>No declared methods</h4>" : getUl("Declared Methods", declaredMethods);
        sb.append(segment);
        sb.append("</div></div></div>");
        return sb.toString();
    }

    private String getUl(String title, List<String> items) {
        StringBuilder sb = new StringBuilder();
        sb.append("<h4>").append(title).append("</h4>");
        sb.append("<ul>");
        for (String i : items) {
            sb.append("<li>").append(i).append("</li>");
        }
        sb.append("</ul>");
        return sb.toString();
    }

    private List<String> getModifiersForClass(Class c) {
        List<String> modifiersList = new ArrayList<>();
        int mod = c.getModifiers();
        fillModifierList(mod, modifiersList);
        return modifiersList;
    }

    private List<String> getModifiersForMember(Member m) {
        List<String> modifiersList = new ArrayList<>();
        int mod = m.getModifiers();
        fillModifierList(mod, modifiersList);
        return modifiersList;
    }

    private void fillModifierList(int mod, List<String> list) {
        if (Modifier.isPublic(mod)) list.add("public");
        if (Modifier.isPrivate(mod)) list.add("private");
        if (Modifier.isAbstract(mod)) list.add("abstract");
        if (Modifier.isFinal(mod)) list.add("final");
        if (Modifier.isAbstract(mod)) list.add("abstract");
        if (Modifier.isProtected(mod)) list.add("protected");
        if (Modifier.isSynchronized(mod)) list.add("synchronized");
        if (Modifier.isTransient(mod)) list.add("transient");
    }

    private List<String> getSuperClasses(Object o) {
        List<String> superClassesList = new ArrayList<>();
        Class c = o.getClass();
        Class superClass = c.getSuperclass();
        while (superClass != null) {
            superClassesList.add(superClass.getName());
            c = superClass;
            superClass = c.getSuperclass();
        }
        return superClassesList;
    }

    private List<String> getDeclaredFields(Class c) {
        List<String> declaredFieldsList = new ArrayList<>();
        Field[] declaredFields = c.getDeclaredFields();
        for (Field f : declaredFields) {
            StringBuilder sb = new StringBuilder();
            getModifiersForMember(f).forEach(m -> sb.append(m).append(" "));
            sb.append(f.getType().getName()).append(" ").append(f.getName());
            declaredFieldsList.add(sb.toString());
        }
        return declaredFieldsList;
    }

    private List<String> getDeclaredMethods(Class c) {
        List<String> declaredMethodsList = new ArrayList<>();
        Method[] declaredMethods = c.getDeclaredMethods();
        for (Method m : declaredMethods) {
            StringBuilder sb = new StringBuilder();
            getModifiersForMember(m).forEach(mod -> sb.append(mod).append(" "));
            sb.append(m.getReturnType().getName()).append(" ").append(m.getName()).append("(");
            Class[] parameters = m.getParameterTypes();
            for (int i = 0; i < parameters.length; i++) {
                if (i == parameters.length - 1) {
                    sb.append(parameters[i].getSimpleName());
                } else {
                    sb.append(parameters[i].getSimpleName() + ", ");
                }
            }
            sb.append(")");
            declaredMethodsList.add(sb.toString());
        }
        return declaredMethodsList;
    }

    private List<String> getConstructors(Class c) {
        List<String> constructorsList = new ArrayList<>();
        String name = c.getSimpleName();
        Constructor[] constructors = c.getConstructors();
        for (Constructor cn : constructors) {
            StringBuilder sb = new StringBuilder();
            getModifiersForMember(cn).forEach(m -> sb.append(m).append(" "));
            sb.append(name).append("(");
            Class[] parameterTypes = cn.getParameterTypes();
            for (int i = 0; i < parameterTypes.length; i++) {
                if (i == parameterTypes.length -1) {
                    sb.append(parameterTypes[i].getSimpleName());
                } else {
                    sb.append(parameterTypes[i].getSimpleName() + ", ");
                }
            }
            sb.append(")");
            constructorsList.add(sb.toString());
        }
        return constructorsList;
    }

    private List<String> getInterfaces(Class c) {
        List<String> interfacesList = new ArrayList<>();
        Class[] interfaces = c.getInterfaces();
        for (Class i : interfaces) {
            interfacesList.add(i.getSimpleName());
        }
        return interfacesList;
    }

}
