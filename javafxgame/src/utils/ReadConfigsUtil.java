package utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadConfigsUtil {

    private static final String filesDirectory = "resources/configTextFiles";

    public static int readIntFromTextFile(String fileName) {
        JndiUtil jndiUtil = new JndiUtil();
        String path = jndiUtil.getFileFromFS(filesDirectory, fileName);
        try {
            String content = readFile(path);
            return Integer.parseInt(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 100;
    }

    public static String readStringFromTextFile(String fileName) {
        JndiUtil jndiUtil = new JndiUtil();
        String path = jndiUtil.getFileFromFS(filesDirectory, fileName);
        try {
            String content = readFile(path);
            return content;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String readFile(String path) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(path));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }

}
