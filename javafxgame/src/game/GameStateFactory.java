package game;

import game.gameObjects.Ball;
import game.gameObjects.Paddle;
import game.gameObjects.Player;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class GameStateFactory {

    private GameStateFactory() {}

    public static GameState getInitialGameState(int finalScore, String p1, String p2) {
        Paddle paddle1 = new Paddle(0,0, Constants.PADDLE_WIDTH,Constants.PADDLE_HEIGHT,Constants.SPEED);
        Paddle paddle2 = new Paddle(0,0,Constants.PADDLE_WIDTH,Constants.PADDLE_HEIGHT,Constants.SPEED);
        Player player1 = new Player(1,0,paddle1, p1);
        Player player2 = new Player(2,0,paddle2, p2);
        Ball ball = new Ball(0,0,Constants.SPEED, Constants.BALL_RADIUS,-1,0);
        return new GameState(player1, player2, ball, finalScore);
    }

    // TODO - make use of this again, have server choose, new game or load previous
    public static GameState loadGameState() {
        // Deserialization
        GameState gameState = null;
        String fileName = "gameState.ser";
        try(FileInputStream file = new FileInputStream(fileName);
            ObjectInputStream in = new ObjectInputStream(file)) {
            gameState = (GameState)in.readObject();
            System.out.println("Object deserialized: " + gameState);
        }
        catch(IOException ex) {
            System.out.println("IOException is caught: " + ex.getMessage());
        }
        catch(ClassNotFoundException ex){
            System.out.println("ClassNotFoundException is caught: " + ex.getMessage());
        }
        return gameState;
    }
}
