package game;

import game.gameObjects.Ball;
import game.gameObjects.Paddle;
import game.gameObjects.Player;
import javafx.scene.input.KeyCode;
import javafx.scene.media.AudioClip;
import org.apache.commons.lang3.SerializationUtils;
import server.GameServer;
import utils.GameTimer;
import XmlReplay.XmlGameRecorder;

import java.rmi.RemoteException;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

public class GameEngine implements Runnable {

    private GameState gameState;
    private GameTimer gameTimer;
    private XmlGameRecorder recorder;

    private Player player1;
    private Player player2;
    private Ball ball;

    private Set<KeyCode> activeKeys;
    private GameServer server;

    public GameEngine(GameState gameState, GameServer server) {
        this.gameState = gameState;
        this.server = server;
        initGame();
        recorder = new XmlGameRecorder();
    }

    private void initGame() {
        initGameObjects();
        gameState.setPaused(true);
        gameState.setMatchEnd(false);

        // holder for player intentions (keys)
        activeKeys = new ConcurrentSkipListSet<>();
        try {
            // reference to the keySet held by the server
            activeKeys = server.getActiveKeys();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        // vars for playing sounds
        gameState.setHitGoal(false);
        gameState.setHitPaddle(false);
        gameState.setHitWall(false);

        startTimer();
    }

    // could access this through gameState, but storing references in extra variables for ease of use
    private void initGameObjects() {
        player1 = gameState.getPlayer1();
        player2 = gameState.getPlayer2();
        ball = gameState.getBall();
    }

    private void updateGame() throws InterruptedException, RemoteException {
        while (!gameState.isGameEnd()) {
            server.dispatchGameState(this.gameState);
            gameTimer.stop();
            Thread.sleep(2000);
            gameTimer.start();
            recorder.SaveGameState(gameTimer.getGameTime(), SerializationUtils.clone(this.gameState));
            while (!gameState.isMatchEnd()) {
                if (gameState.isPaused()) {
                    gameTimer.stop();
                    while (gameState.isPaused()) {
                        Thread.sleep(100);
                        checkIfUnpaused();
                    }
                    gameTimer.start();
                } else {
                    updateGameState();
                    server.dispatchGameState(this.gameState);
                    recorder.SaveGameState(gameTimer.getGameTime(), SerializationUtils.clone(this.gameState));
                    playSoundsIfNeeded();
                    Thread.sleep(22);
                }
                checkIfPaused();
            }
            gameState.setMatchEnd(false);
            gameState.setGameObjectsToInitialPositions();
            playSoundsIfNeeded();
        }
        server.dispatchGameState(this.gameState);
        recorder.SaveGameState(gameTimer.getGameTime(), SerializationUtils.clone(this.gameState));
        gameTimer.shutDown();
        playSoundsIfNeeded();
        recorder.SaveStatesToFile();
    }

    // updating state
    private void checkIfPaused() {
        gameState.setPaused(activeKeys.contains(KeyCode.ENTER));
    }

    private void checkIfUnpaused() {
        gameState.setPaused(!activeKeys.contains(KeyCode.R));
    }

    private void updateGameState() {
        checkIfPaused();
        updateBallState();
        updatePaddlesState();
    }

    private void updateBallState() {
        double currentX = ball.getPosX();
        double currentY = ball.getPosY();
        double nextX = currentX + (ball.getDirX() * ball.getSpeed());
        double nextY = currentY + (ball.getDirY() * ball.getSpeed());
        ball.setPosX(currentX + (ball.getDirX() * ball.getSpeed()));
        ball.setPosY(currentY + (ball.getDirY() * ball.getSpeed()));
        if (ballHitTopWall(nextY) || ballHitBottomWall(nextY)) {
            ball.setDirY(ball.getDirY() * -1);
            gameState.setHitWall(true);
            if (ballHitTopWall(nextY)) {
                ball.setPosY(0);
            } else {
                ball.setPosY(Constants.WINDOW_HEIGHT - ball.getRadius());
            }
        } else if (ballHitLeftPlayer(nextX, nextY) || ballHitRightPlayer(nextX, nextY)) {
            gameState.setHitPaddle(true);
            ball.setDirX(ball.getDirX() * -1);
            if(ballHitLeftPlayer(nextX, nextY)) {
                ball.setPosX(player1.getPaddle().getPosX() + player1.getPaddle().getWidth());
                ball.setDirY(calculateNewDirectionY(player1));
            } else {
                ball.setPosX(player2.getPaddle().getPosX() - ball.getRadius());
                ball.setDirY(calculateNewDirectionY(player2));
            }
        } else if(ballHitLeftGoal(nextX) || ballHitRightGoal(nextX)) {
            gameState.setHitGoal(true);
            gameState.setMatchEnd(true);
            if (ballHitLeftGoal(nextX)) {
                player2.setScore(player2.getScore() + 1);
            } else {
                player1.setScore(player1.getScore() + 1);
            }
        }
    }

    /* COLLISION DETECTION */

    private void updatePaddlesState() {
        if (activeKeys.contains(KeyCode.W)) updatePlayerPosition(player1.getPaddle(), -1);
        if (activeKeys.contains((KeyCode.S))) updatePlayerPosition(player1.getPaddle(), 1);
        if (activeKeys.contains(KeyCode.UP)) updatePlayerPosition(player2.getPaddle(), -1);
        if (activeKeys.contains(KeyCode.DOWN)) updatePlayerPosition(player2.getPaddle(), 1);
    }

    private void updatePlayerPosition(Paddle paddle, int direction) {
        double currentY = paddle.getPosY();
        double nextY = currentY + direction * paddle.getSpeed();
        if (paddleHitTopWall(nextY)) {
            paddle.setPosY(0);
        } else if(paddleHitBottomWall(nextY)) {
            paddle.setPosY(Constants.WINDOW_HEIGHT - paddle.getHeight());
        } else {
            paddle.setPosY(nextY);
        }
    }

    /* COLLISION DETECTION HELPERS */

    private boolean ballHitTopWall(double nextY) {
        return nextY <= 0;
    }

    private boolean ballHitBottomWall(double nextY) {
        return nextY  + ball.getRadius() >= Constants.WINDOW_HEIGHT;
    }

    private boolean ballHitLeftGoal(double nextX) {
        return nextX <= 0;
    }

    private boolean ballHitRightGoal(double nextX) {
        return nextX + ball.getRadius() >= Constants.WINDOW_WIDTH;
    }

    private boolean ballHitLeftPlayer(double nextX, double nextY) {
        Paddle paddle = player1.getPaddle();
        return nextX < paddle.getPosX() + paddle.getWidth()
                && nextY + ball.getRadius() > paddle.getPosY()
                && nextY < paddle.getPosY() + player1.getPaddle().getHeight();
    }

    private boolean ballHitRightPlayer(double nextX, double nextY) {
        Paddle paddle = player2.getPaddle();
        return nextX + ball.getRadius() > paddle.getPosX()
                && nextY + ball.getRadius() > paddle.getPosY()
                && nextY < paddle.getPosY() + player1.getPaddle().getHeight();
    }

    private double calculateNewDirectionY(Player player) {
        Paddle paddle = player.getIndex() == 1 ? player1.getPaddle() : player2.getPaddle();
        double ballCenter = ball.getPosY() + (ball.getRadius() / 2);
        double topQuarter = paddle.getPosY() + paddle.getHeight() / 4;
        double middle = paddle.getPosY() + paddle.getHeight() / 2;
        double bottomQuarter = paddle.getPosY() + paddle.getHeight() / 2 + paddle.getHeight() / 4;
        if (ballCenter == middle) {
            return 0;
        } else if(ballCenter > topQuarter && ballCenter < middle) {
            return -0.5;
        } else if (ballCenter < topQuarter) {
            return -0.9;
        } else if (ballCenter > middle && ballCenter < bottomQuarter) {
            return 0.5;
        } else {
            return 0.9;
        }
    }

    private boolean paddleHitTopWall(double nextY) {
        return nextY <= 0;
    }

    private boolean paddleHitBottomWall(double nextY) {
        return nextY + player1.getPaddle().getHeight() >= Constants.WINDOW_HEIGHT;
    }

    private void playSoundsIfNeeded() {
        if (gameState.isGameEnd()) {
            new AudioClip(Constants.SOUND_VICTORY).play(100);
            return;
        }
        String sound = null;
        if (gameState.isHitPaddle()) {
            sound = Constants.SOUND_PADDLE;
            gameState.setHitPaddle(false);
        } else if (gameState.isHitWall()) {
            sound = Constants.SOUND_WALL;
            gameState.setHitWall(false);
        } else if (gameState.isHitGoal()) {
            sound = Constants.SOUND_GOAL;
            gameState.setHitGoal(false);
        }
        if (sound != null) {
            new AudioClip(sound).play();
        }
    }

    private void startTimer() {
        this.gameTimer = new GameTimer(server);
        Thread timerThread = new Thread(this.gameTimer);
        timerThread.setDaemon(true);
        timerThread.start();
    }

    @Override
    public void run() {
        try {
            updateGame();
        } catch (RemoteException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
