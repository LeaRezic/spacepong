package game;

import javafx.scene.paint.Color;

public class Constants {

    public static final double PADDLE_WIDTH = 20;
    public static final double PADDLE_HEIGHT = 120;
    public static final double BALL_RADIUS = 20;
    public static final double WINDOW_WIDTH = 800;
    public static final double WINDOW_HEIGHT = 500;
    public static final int SPEED = 10;
    public static final Color PADDLE_COLOR = Color.BURLYWOOD;
    public static final Color BALL_COLOR = Color.LIMEGREEN;

    // TODO - could be nice to add a helper class that plays a sound, given only a relative path
    public static final String SOUND_GOAL = "file:///C:/Users/Oradian/Documents/Faks/JAVA2/project/SpacePongRepo/javafxgame/src/resources/sounds/goal.wav";
    public static final String SOUND_WALL = "file:///C:/Users/Oradian/Documents/Faks/JAVA2/project/SpacePongRepo/javafxgame/src/resources/sounds/wall.wav";
    public static final String SOUND_PADDLE = "file:///C:/Users/Oradian/Documents/Faks/JAVA2/project/SpacePongRepo/javafxgame/src/resources/sounds/paddle.wav";
    public static final String SOUND_BACKGROUND = "file:///C:/Users/Oradian/Documents/Faks/JAVA2/project/SpacePongRepo/javafxgame/src/resources/sounds/background.wav";
    public static final String SOUND_VICTORY = "file:///C:/Users/Oradian/Documents/Faks/JAVA2/project/SpacePongRepo/javafxgame/src/resources/sounds/victory.wav";

}
