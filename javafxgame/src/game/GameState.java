package game;

import game.gameObjects.Ball;
import game.gameObjects.Player;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.FIELD)
public class GameState implements Serializable {

    @XmlElement(name = "Player1")
    private Player player1;
    @XmlElement(name = "Player2")
    private Player player2;
    @XmlElement(name = "Ball")
    private Ball ball;
    @XmlElement(name = "finalScore")
    private int finalScore;

    @XmlElement(name = "paused")
    private boolean paused;
    @XmlElement(name = "matchEnd")
    private boolean matchEnd;
    @XmlElement(name = "hitWall")
    private boolean hitWall;
    @XmlElement(name = "hitPaddle")
    private boolean hitPaddle;
    @XmlElement(name = "hitGoal")
    private boolean hitGoal;

    public GameState(Player player1, Player player2, Ball ball, int finalScore) {
        this.player1 = player1;
        player1.setScore(0);
        this.player2 = player2;
        player2.setScore(0);
        this.ball = ball;
        this.finalScore = finalScore;
        setGameObjectsToInitialPositions();
        paused = true;
        matchEnd = false;
        hitGoal = false;
        hitWall = false;
        hitPaddle = false;
    }

    public GameState() {
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public Ball getBall() {
        return ball;
    }

    public boolean isGameEnd() {
        return player1.getScore() >= finalScore || player2.getScore() >= finalScore;
    }

    public boolean isPaused() {
        return paused;
    }

    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    public boolean isMatchEnd() {
        return matchEnd;
    }

    public void setMatchEnd(boolean matchEnd) {
        this.matchEnd = matchEnd;
    }

    public boolean isHitWall() {
        return hitWall;
    }

    public void setHitWall(boolean hitWall) {
        this.hitWall = hitWall;
    }

    public boolean isHitPaddle() {
        return hitPaddle;
    }

    public void setHitPaddle(boolean hitPaddle) {
        this.hitPaddle = hitPaddle;
    }

    public boolean isHitGoal() {
        return hitGoal;
    }

    public void setHitGoal(boolean hitGoal) {
        this.hitGoal = hitGoal;
    }

    public void setGameObjectsToInitialPositions() {
        double p1x = 5;
        double p2x = Constants.WINDOW_WIDTH - player1.getPaddle().getWidth();
        double py = Constants.WINDOW_HEIGHT / 2 - (player1.getPaddle().getHeight() / 2);
        double bx = Constants.WINDOW_WIDTH / 2 - (ball.getRadius() / 2);
        double by = Constants.WINDOW_HEIGHT / 2 - (ball.getRadius() / 2);
        player1.getPaddle().setPosX(p1x);
        player1.getPaddle().setPosY(py);
        player2.getPaddle().setPosX(p2x);
        player2.getPaddle().setPosY(py);
        ball.setPosX(bx);
        ball.setPosY(by);
        ball.setDirX(1);
        ball.setDirY(0);
    }

}
