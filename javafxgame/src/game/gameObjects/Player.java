package game.gameObjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.FIELD)
public class Player implements Serializable {

    @XmlElement(name = "playerIndex")
    private int index;
    @XmlElement(name = "playerScore")
    private int score;
    @XmlElement(name = "playerName")
    private String name;
    @XmlElement(name = "playerPaddle")
    private Paddle paddle;

    public Player(int index, int score, Paddle paddle) {
        this.index = index;
        this.score = score;
        this.paddle = paddle;
        this.name = "P " + index;
    }

    public Player(int index, int score, Paddle paddle, String name) {
        this.index = index;
        this.score = score;
        this.paddle = paddle;
        this.name = name;
    }

    public Player() {
    }

    public int getIndex() {
        return index;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Paddle getPaddle() {
        return paddle;
    }

    public String getName() {
        return name;
    }

}
