package game.gameObjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.FIELD)
public class MovingObject implements Serializable {
    @XmlElement(name = "posX")
    private double posX;
    @XmlElement(name = "posY")
    private double posY;
    @XmlElement(name = "width")
    private double width;
    @XmlElement(name = "height")
    private double height;
    @XmlElement(name = "speed")
    private double speed;

    public MovingObject(double posX, double posY, double width, double height, double speed) {
        this.posX = posX;
        this.posY = posY;
        this.width = width;
        this.height = height;
        this.speed = speed;
    }

    public MovingObject() {
    }

    public double getPosX() {
        return posX;
    }

    public double getPosY() {
        return posY;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double getSpeed() {
        return speed;
    }

    public void setPosX(double posX) {
        this.posX = posX;
    }

    public void setPosY(double posY) {
        this.posY = posY;
    }

}