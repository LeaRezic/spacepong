package game.gameObjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Ball extends MovingObject {

    @XmlElement(name = "radius")
    private double radius;
    @XmlElement(name = "dirX")
    private double dirX;
    @XmlElement(name = "dirY")
    private double dirY;

    public Ball(double posX, double posY, double speed, double radius, double dirX, double dirY) {
        super(posX, posY, radius, radius, speed);
        this.radius = radius;
        this.dirX = dirX;
        this.dirY = dirY;
    }

    public Ball() {
    }

    public double getRadius() {
        return radius;
    }

    public double getDirX() {
        return dirX;
    }

    public void setDirX(double dirX) {
        this.dirX = dirX;
    }

    public double getDirY() {
        return dirY;
    }

    public void setDirY(double dirY) {
        this.dirY = dirY;
    }

}
