package game.gameObjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class Paddle extends MovingObject {

    public Paddle(double posX, double posY, double width, double height, double speed) {
        super(posX, posY, width, height, speed);
    }

    public Paddle() {
    }

}
