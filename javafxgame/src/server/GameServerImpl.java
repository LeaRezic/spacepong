package server;

import client.GameClient;
import game.GameState;
import javafx.scene.input.KeyCode;
import utils.ReadConfigsUtil;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

public class GameServerImpl implements GameServer {

    private List<String> gameClientNames;
    private List<GameClient> gameClients;
    private Set<KeyCode> activeKeys = new ConcurrentSkipListSet<>();
    private boolean readyToStart = false;
    private int registryPort;
    private int groupSocketPort;
    private String groupSocketAddress;

    GameServerImpl() {
        gameClientNames = new ArrayList<>();
        gameClients = new ArrayList<>();
        readConfigs();
    }

    private void readConfigs() {
        groupSocketAddress = ReadConfigsUtil.readStringFromTextFile("groupSocketAddress");
        groupSocketPort = ReadConfigsUtil.readIntFromTextFile("groupSocketPort");
        registryPort = ReadConfigsUtil.readIntFromTextFile("registryPort");
    }

    private void registerClient(String ClientName) {
        if (gameClientNames.size() < 2) {
            gameClientNames.add(ClientName);
            System.out.println("Adding client " + ClientName + " to names lit.");
        }
        if (gameClientNames.size() == 2) {
            lookupClients();
            readyToStart = true;
        }
    }

    private void lookupClients() {
        try {
            Registry registry = LocateRegistry.getRegistry(registryPort);
            GameClient firstPlayer = (GameClient) registry.lookup(gameClientNames.get(0));
            gameClients.add(firstPlayer);
            System.out.println("Added player: " + firstPlayer.getName());
            GameClient secondPlayer = (GameClient) registry.lookup(gameClientNames.get(1));
            gameClients.add(secondPlayer);
            System.out.println("Added player: " + secondPlayer.getName());
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    boolean isReadyToStart() {
        return readyToStart;
    }

    List<String> getGameClientNames() {
        return gameClientNames;
    }

    @Override
    public Set<KeyCode> getActiveKeys() {
        return activeKeys;
    }

    @Override
    public void giveKey(String clientName, KeyCode event) throws RemoteException {
        switch (event) {
            case UP:
                if (clientName.equals(gameClientNames.get(0))) {
                    activeKeys.add(KeyCode.W);
                } else {
                    activeKeys.add(event);
                }
                break;
            case DOWN:
                if (clientName.equals(gameClientNames.get(0))) {
                    activeKeys.add(KeyCode.S);
                } else {
                    activeKeys.add(event);
                }
                break;
            case ENTER:
                activeKeys.add(event);
                break;
            case R:
                activeKeys.add(event);
                break;
            default:
                break;
        }
    }

    @Override
    public void removeKey(String clientName, KeyCode event) throws RemoteException {
        switch (event) {
            case UP:
                if (clientName.equals(gameClientNames.get(0))) {
                    activeKeys.remove(KeyCode.W);
                } else {
                    activeKeys.remove(event);
                }
                break;
            case DOWN:
                if (clientName.equals(gameClientNames.get(0))) {
                    activeKeys.remove(KeyCode.S);
                } else {
                    activeKeys.remove(event);
                }
                break;
            case ENTER:
                activeKeys.remove(event);
                break;
            case R:
                activeKeys.remove(event);
                break;
            default:
                break;
        }
    }

    @Override
    public void dispatchGameState(GameState state) throws RemoteException {
        // TODO - remove, testing jaxb marshalling of state
        /*try {
            JAXBContext jaxbContext = JAXBContext.newInstance(GameState.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(state, System.out);
        } catch (JAXBException e) {
            e.printStackTrace();
        }*/

        gameClients.forEach(c -> {
            try {
                c.receiveGameState(state);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void getRegistered(String clientName) throws RemoteException {
        registerClient(clientName);
    }

    @Override
    public void dispatchMatchTime(String time) {
        try {
            MulticastSocket socket = new MulticastSocket();
            InetAddress groupAddress = InetAddress.getByName(groupSocketAddress);
            byte[] bytes = time.getBytes();
            DatagramPacket packet = new DatagramPacket(bytes, bytes.length, groupAddress, groupSocketPort);
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
