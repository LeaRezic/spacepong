package server;

import game.GameState;
import javafx.scene.input.KeyCode;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Set;

public interface GameServer extends Remote {
    void giveKey(String clientName, KeyCode event) throws RemoteException;
    void removeKey(String clientName, KeyCode event) throws RemoteException;
    void dispatchGameState(GameState state) throws RemoteException;
    void getRegistered(String clientName) throws RemoteException;
    void dispatchMatchTime(String time) throws  RemoteException;
    Set<KeyCode> getActiveKeys() throws RemoteException;
}
