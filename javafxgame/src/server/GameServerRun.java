package server;

import XmlReplay.XmlGamePlayer;
import game.GameEngine;
import game.Constants;
import game.GameStateFactory;
import game.GameState;
import javafx.scene.media.AudioClip;
import utils.ReadConfigsUtil;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

// TODO - clean up lots :D
public class GameServerRun {
    public static void main(String[] args) throws RemoteException, InterruptedException {

        // reading configs from configTextFiles dir
        int registryPort = ReadConfigsUtil.readIntFromTextFile("registryPort");
        String serverName = ReadConfigsUtil.readStringFromTextFile("serverName");
        int finalScore = ReadConfigsUtil.readIntFromTextFile("finalResult");

        // instantiate and register game server on registry
        GameServerImpl gameServer = new GameServerImpl();
        Registry registry = LocateRegistry.createRegistry(registryPort);
        GameServer stub = (GameServer) UnicastRemoteObject.exportObject(gameServer, 0);
        registry.rebind(serverName, stub);

        System.out.println("Game server ready");

        // choose - load, start new, replay last
        Scanner scanner = new Scanner(System.in);
        String option;
        System.out.println("Choose action:\n1. Play new game\n2. Load saved game\n3. Replay last 10 seconds of last game");
        System.out.println("Enter number of option:");
        option = scanner.nextLine();

        Thread program;

        if (option.equals("3")) {
            XmlGamePlayer replay = new XmlGamePlayer(gameServer);
            program = new Thread(replay);
            while (!gameServer.isReadyToStart()) {
                Thread.sleep(1000);
                System.out.println("Waiting for clients to register...");
            }
            program.setDaemon(true);
            program.start();
        } else if (option.equals("1")) {
            // sleeping until server is ready (both clients have registered)
            while (!gameServer.isReadyToStart()) {
                Thread.sleep(1000);
                System.out.println("Waiting for clients to register...");
            }

            // initializing the game state and starting the game engine
            System.out.println("Clients registered!\nInitializing the game...");
            String player1 = gameServer.getGameClientNames().get(0);
            String player2 = gameServer.getGameClientNames().get(1);
            GameState state = GameStateFactory.getInitialGameState(finalScore, player1, player2);

            System.out.println("Game state initialized, starting the engine...");
            GameEngine engine = new GameEngine(state, gameServer);
            Thread gameRun = new Thread(engine);
            gameRun.setDaemon(true);
            gameRun.start();

            // setting game state's paused to false - game can start, both clients are online
            state.setPaused(false);
        }

        // playing the background music (not the best place for this, but whatever)
        AudioClip backgroundMusic = new AudioClip(Constants.SOUND_BACKGROUND);
        backgroundMusic.setCycleCount(AudioClip.INDEFINITE);
        backgroundMusic.play();

    }
}
