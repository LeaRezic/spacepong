package XmlReplay;

import server.GameServer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.rmi.RemoteException;
import java.util.LinkedList;

public class XmlGamePlayer implements Runnable {

    private GameServer server;
    private XmlSavedGame replay;
    private static final String PATH = "savedGameStates.xml";

    public XmlGamePlayer(GameServer server) {
        this.server = server;
        replay = new XmlSavedGame();
        initialize();
    }

    private void initialize() {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(XmlSavedGame.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            replay = (XmlSavedGame) jaxbUnmarshaller.unmarshal(new File(PATH));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        if (replay.getGameStatesSaved().size() <= 0) {
            throw new Error("Tried to load games, but there seems to be nothing recorded...");
        }
        LinkedList<XmlGameStateSave> gameStates = replay.getGameStatesSaved();
        while (gameStates.size() > 0) {
            XmlGameStateSave state = gameStates.removeFirst();
            try {
                server.dispatchGameState(state.getState());
                server.dispatchMatchTime(state.getTimeStamp());
                Thread.sleep(33);
            } catch (RemoteException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
