package XmlReplay;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;

@XmlRootElement(name = "XmlSavedGame")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlSavedGame {

    @XmlElement(name = "XmlGameStateSave")
    public LinkedList<XmlGameStateSave> gameStatesSaved;

    public XmlSavedGame() {
    }

    public XmlSavedGame(LinkedList<XmlGameStateSave> gameStatesSaved) {
        this.gameStatesSaved = gameStatesSaved;
    }

    public LinkedList<XmlGameStateSave> getGameStatesSaved() {
        return gameStatesSaved;
    }
}
