package XmlReplay;

import game.GameState;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.LinkedList;

public class XmlGameRecorder {

    private static final int MAX_ENTRIES = 500;
    private LinkedList<XmlGameStateSave> savedStates;
    private static final String PATH = "savedGameStates.xml";

    public XmlGameRecorder() {
        this.savedStates = new LinkedList<>();
    }

    public void SaveGameState(String timeStamp, GameState state) {
            XmlGameStateSave savedState = new XmlGameStateSave();
            savedState.setState(state);
            savedState.setTimeStamp(timeStamp);
            savedStates.add(savedState);
            if (savedStates.size() >= MAX_ENTRIES) {
                savedStates.removeFirst();
            }
    }

    public void SaveStatesToFile() {
        XmlSavedGame replayRecord = new XmlSavedGame(savedStates);
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(XmlSavedGame.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(replayRecord, new File(PATH));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
