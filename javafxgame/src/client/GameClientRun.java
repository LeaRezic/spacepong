package client;

import game.Constants;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import server.GameServer;
import client.GUI.GameScreen;
import utils.ReadConfigsUtil;
import utils.ReflectionUtil;
import utils.TimerSocketListener;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

public class GameClientRun extends Application {

    @Override
    public void start(Stage primaryStage) {

        // GETTING THE GAME SCREEN
        FXMLLoader gameScreenLoader = new FXMLLoader(getClass().getResource("GUI/gameScreen.fxml"));
        Parent rootGameScreen = null;
        try {
            rootGameScreen = gameScreenLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        rootGameScreen.getStylesheets().add(this.getClass().getResource("GUI/gameScreen.css").toExternalForm());
        Scene gameScreenScene = new Scene(rootGameScreen, Constants.WINDOW_WIDTH -5, Constants.WINDOW_HEIGHT -8);
        GameScreen gameScreenController = gameScreenLoader.getController();

        // GETTING SERVER
        String serverName = ReadConfigsUtil.readStringFromTextFile("serverName");
        int registryPort = ReadConfigsUtil.readIntFromTextFile("registryPort");
        Registry registry = null;
        GameServer server = null;
        while (server == null) {
            try {
                System.out.println("Getting server...");
                registry = LocateRegistry.getRegistry(registryPort);
                server = (GameServer) registry.lookup(serverName);
            } catch (RemoteException | NotBoundException e) {
                System.out.println("No server yet...");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }

        // SETTING UP CLIENT
        Scanner scanner = new Scanner(System.in);
        String name;
        System.out.println("Enter player name:");
        name = scanner.nextLine();
        GameClient client = new GameClientImpl(name, gameScreenController, server);

        // RUNNING THE SCREEN MANAGER
        ScreenManager screenManager = new ScreenManager(primaryStage, gameScreenScene, gameScreenController);
        screenManager.showGameScreen();

        // LISTENING TO KEYS - on press -> add to servers key list, on release -> remove
        GameServer finalServer = server;
        gameScreenScene.setOnKeyPressed((KeyEvent event) -> {
            try {
                finalServer.giveKey(name, event.getCode());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        });
        gameScreenScene.setOnKeyReleased((KeyEvent event) -> {
            try {
                finalServer.removeKey(name, event.getCode());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        });

        // LISTENING TO GROUP SOCKET FOR TIMER
        Thread timerSetter = new Thread(new TimerSocketListener(gameScreenController.getLabel()));
        timerSetter.setDaemon(true);
        timerSetter.start();

        // REGISTERING TO REGISTRY
        System.out.println("Registering client to local registry...");
        try {
            GameClient stub = (GameClient) UnicastRemoteObject.exportObject(client, 0);
            registry.rebind(name, stub);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        // REGISTERING ON SERVER
        System.out.println("Registering client on server...");
        try {
            client.registerOnServer();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        // REFLECTION
        ReflectionUtil ref = new ReflectionUtil();
        ref.generateHtmlDetails();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
