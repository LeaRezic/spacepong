package client;

import game.GameState;
import server.GameServer;
import client.GUI.GameScreen;

import java.rmi.RemoteException;

public class GameClientImpl implements GameClient {

    private String name;
    private GameScreen gameController;
    private GameServer server;

    public GameClientImpl(String name, GameScreen gameController, GameServer server) {
        this.name = name;
        this.gameController = gameController;
        this.server = server;
    }

    @Override
    public void registerOnServer() throws RemoteException {
        System.out.println("Getting registered on server...");
        server.getRegistered(name);
    }

    @Override
    public String getName() throws RemoteException {
        return name;
    }

    @Override
    public void receiveGameState(GameState state) throws RemoteException {
        gameController.drawGame(state);
    }
}
