package client.GUI;

import client.ScreenManager;
import game.Constants;
import game.gameObjects.Ball;
import game.GameState;
import game.gameObjects.Paddle;
import game.gameObjects.Player;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Control;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.net.URL;
import java.util.*;

public class GameScreen implements Initializable {

    @FXML
    private Hyperlink linkSaveAndExit;
    @FXML
    private Hyperlink btnExit;
    @FXML
    private Label textWinner;
    @FXML
    private Label textAwesome;
    @FXML
    private Label lblWinner;
    @FXML
    private Canvas canvas;
    @FXML
    private Label scoreP1;
    @FXML
    private Label scoreP2;
    @FXML
    private Label lblp1name;
    @FXML
    private Label lblp2name;
    @FXML
    private Label lblTimer;

    private SimpleStringProperty player1Score;
    private SimpleStringProperty player2Score;
    private SimpleStringProperty player1Name;
    private SimpleStringProperty player2Name;
    private SimpleStringProperty winner;
    private List<Control> visibilitySwitchControls;
    private ScreenManager screenManager;
    private GameState innerGameState;

    public GameScreen() {
    }

    public void drawGame(GameState gameState) {
        if (this.innerGameState == null) {
            this.innerGameState = gameState;
            Platform.runLater(() -> {
                player1Name.setValue(gameState.getPlayer1().getName().toUpperCase());
                player2Name.setValue(gameState.getPlayer2().getName().toUpperCase());
            });
        }
        Player player1 = gameState.getPlayer1();
        Player player2 = gameState.getPlayer2();
        Platform.runLater(() -> {
            if (!gameState.isGameEnd()) {
                player1Score.setValue(Integer.toString(player1.getScore()));
                player2Score.setValue(Integer.toString(player2.getScore()));
                GameScreen.this.drawObjects(player1.getPaddle(), player2.getPaddle(), gameState.getBall());
            } else {
                // screenManager.stopBackgroundMusic();
                player1Score.setValue(Integer.toString(player1.getScore()));
                player2Score.setValue(Integer.toString(player2.getScore()));
                String winnerName = player1.getScore() > player2.getScore() ? player1.getName() : player2.getName();
                drawGameEnd(winnerName.toUpperCase());
            }
        });
    }

    private void drawGameEnd(String winnerName) {
        winner.setValue(winnerName);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.clearRect(0,0,Constants.WINDOW_WIDTH,Constants.WINDOW_HEIGHT);
        switchVisibilityForControls(true);
        linkSaveAndExit.setVisible(false);
    }

    private void drawObjects(Paddle player1, Paddle player2, Ball ball) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.clearRect(0,0,Constants.WINDOW_WIDTH,Constants.WINDOW_HEIGHT);
        drawPaddle(gc, player1);
        drawPaddle(gc, player2);
        drawBall(gc, ball);
    }

    private void drawBall(GraphicsContext gc, Ball ball) {
        gc.setFill(Constants.BALL_COLOR);
        gc.fillOval(ball.getPosX(), ball.getPosY(), ball.getRadius(), ball.getRadius());
    }

    private void drawPaddle(GraphicsContext gc, Paddle paddle) {
        gc.setFill(Constants.PADDLE_COLOR);
        gc.fillRoundRect(paddle.getPosX(), paddle.getPosY(),paddle.getWidth(),paddle.getHeight(),15,15);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        innerGameState = null;
        setToInitialState();
    }

    public void setToInitialState() {
        player1Score = new SimpleStringProperty("0");
        player2Score = new SimpleStringProperty("0");
        player1Name = new SimpleStringProperty("P1");
        player2Name = new SimpleStringProperty("P2");
        winner = new SimpleStringProperty("1");
        visibilitySwitchControls = new ArrayList<>();
        bindTextValues();
        fillVisibilitySwitchControlsList();
        switchVisibilityForControls(false);
    }

    private void switchVisibilityForControls(boolean visible) {
        visibilitySwitchControls.forEach(control -> control.setVisible(visible));
    }

    private void fillVisibilitySwitchControlsList() {
        visibilitySwitchControls.add(textWinner);
        visibilitySwitchControls.add(lblWinner);
        visibilitySwitchControls.add(textAwesome);
        visibilitySwitchControls.add(btnExit);
    }

    private void bindTextValues() {
        scoreP1.textProperty().bind(player1Score);
        scoreP2.textProperty().bind(player2Score);
        lblp1name.textProperty().bind(player1Name);
        lblp2name.textProperty().bind(player2Name);
        lblWinner.textProperty().bind(winner);
    }

    public void playAgain(ActionEvent actionEvent) {
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.close();
    }

    public void exitGame(ActionEvent actionEvent) {
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.close();
    }

    public void setScreenManager(ScreenManager screenManager) {
        this.screenManager = screenManager;
    }

    public void saveAndExit(ActionEvent actionEvent) {
        screenManager.saveGame(innerGameState);
    }

    public Label getLabel() {
        return lblTimer;
    }

}
