package client;

import game.GameState;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface GameClient extends Remote {
    String getName() throws RemoteException;
    void receiveGameState(GameState state) throws RemoteException;
    void registerOnServer() throws RemoteException;
}
