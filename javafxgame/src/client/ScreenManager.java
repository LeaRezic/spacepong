package client;

import game.GameState;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import client.GUI.GameScreen;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;


public class ScreenManager {

    private Stage stage;
    private Scene gameScene;
    private GameScreen gameScreenController;

    public ScreenManager() {
    }

    public ScreenManager(Stage stage, Scene gameScene, GameScreen gameScreenController) {
        this.stage = stage;
        this.gameScene = gameScene;
        this.gameScreenController = gameScreenController;
    }

    /*public void stopBackgroundMusic() {
        backgroundMusic.stop();
    }*/

    public void showGameScreen() {
        gameScreenController.setScreenManager(this);
        stage.setScene(gameScene);
        stage.setTitle("SPACE PONG");
        stage.getIcons().add(new Image("resources/images/ping-pong-icon.png"));
        stage.setResizable(false);
        stage.show();
    }

    public void saveGame(GameState gameState) {
        String fileName = "gameState.ser";
        try (FileOutputStream file = new FileOutputStream(fileName);
             ObjectOutputStream out = new ObjectOutputStream(file)) {
            out.writeObject(gameState);
            System.out.println("Object serialized: " + gameState);
        }
        catch(IOException ex) {
            System.out.println("IOException is caught: " + ex.getMessage());
        }
    }
}
