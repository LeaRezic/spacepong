Java 2 - project assignment
======

Making a game in JavaFX - SPACE PONG

### How to play:
It's multiplayer, to play the game, on must run the Server (GameServerRun) and 2 Clients (GameClientRun).

Server offers 3 options:
1) play a new game
2) load the last saved game
3) replay the last played game

Game goes on until one of the players reaches the final score, which is defined in a text file (resources/configTestFiles/finalScore.txt)
<br>To make save, player must press on the "Save game" button.

Keys:
* Go up: UP
* Go down: DOWN
* Pause: ENTER
* Resume: R


### Includes:
* **JavaFX**: the client GUI
* **Serialization**: saving and loading the game from a serialized gameState
* **Threads and synchronization**: a timer, which can be stopped and resumed depending on gameState
* **JNDI**: reading configs from text files on the file system
* **RMI**: most communication between server and client
* **Sockets**: server dispatching the match time to the clients
* **XML**: recording the last cca 10-15 seconds of the game and replaying it
